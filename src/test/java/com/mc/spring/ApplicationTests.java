package com.mc.spring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

	 private static final Logger logger = LoggerFactory.getLogger(ApplicationTests.class);
	
	@Test
	public void contextLoads() {
	}

	@Test
	public void testlogs() {
//		logger.info("测试日志打印");
		logger.error("测试日志打印");
		
		try{
			Integer a = 1/0;
			a.toString();
		}catch (Exception e) {
			logger.error("执行出错",e);
		}
	}
}
